package consola;

import java.util.Scanner;

public class EjecutableConsolaJava {

	public static void main(String[] args) {
Scanner teclado = new Scanner (System.in);
		
		//MENU
		int numeroMenu;
		String cadena1;
		String cadena4;
		do {
		
		System.out.println("Elige alguna de las opciones que vienen a continuaci�n introduciendo un n�mero:");
		System.out.println("1-Opcion 1");
		System.out.println("2-Opcion 2");
		System.out.println("3-Opcion 3");
		System.out.println("4-Opcion 4");
		
		numeroMenu = teclado.nextInt();
		teclado.nextLine();
		
		
		switch (numeroMenu) { 
		case 1:	
			System.out.println("Introduce una cadena");
			cadena1 = teclado.nextLine();
			System.out.println(invertirCadena(cadena1));
		break;

		case 2:
			System.out.println("mostramos el numero mayor");
			System.out.println(comparacionMax(9, 50));
		break;	
	
		case 3:
			System.out.println("Mostramos numero aleatorio hasta el numero introducido");
			System.out.println(aleatorio(60));
		break;
		case 4:
			System.out.println("escribe una cadena");
			
			System.out.println("Mostramos el desfase de la cadena tantas posiciones como hayamos introducido:");
			System.out.println(cifrar("abcd", 3));
		}
		}while(numeroMenu<4);
		teclado.close();


	}
	
	public static String invertirCadena(String cadena) {
		
		String cadenaInvertida="";
		char caracter;
		for (int i = cadena.length()-1; i >= 0; i--) {
			
			caracter = cadena.charAt(i);
			
			cadenaInvertida = cadenaInvertida+caracter;

		}
		return cadenaInvertida;
		
	}
	
	public static int comparacionMax (int numero1,int numero2) {
		if(numero1>numero2) {
			return numero1;
		
		}else {
			return numero2;
		}
	}
	
	public static int aleatorio (int fin) {
		return (int)(Math.random()*fin);
	}
	

	
	public static String cifrar (String cadena, int desfase) {
		
		
		int resultado;
		char resultadoCadena;
		String resultadoFinal="";
	
		for (int i = 0; i < cadena.length(); i++) {
			
			
			resultado =  (int)cadena.charAt(i)+desfase;
			
			resultadoCadena = (char)resultado;
			resultadoFinal = resultadoFinal+resultadoCadena+"";
		}
		
		return resultadoFinal;

}
	

}
