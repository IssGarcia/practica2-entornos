package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		/* en primer lugar se pide mediante scanner que se introduzca un n�mero(int), desp�es entrar� en el bucle. En primer lugar la i toma el
		 * valor de 1. Pasa al if en donde se comprueba si el resto de la divisi�n del n�mero introducido por teclado y el valor de la i(1) es
		 * igual a cero. Si lo es, se sumar� un +1 a la variable cantidadDvisores, si no lo es no se sumar� nada. Despu�s vuelve a empezar el bucle
		 * pero ahora la variable i tomar� el valor de 2. Se introduce de nuevo en la rutina de lo explicado anteriormente: el numero introducido
		 * es dividido entre dos, si el resto es igual a cero, se sumar� +1 en la variable cantidad de divisores, si no, no se almacenar� nada.
		 * Seguir�n realizando divisiones sucesivas hasta que i tome el valor del n�mero introducido por teclado.
		 * Cuando finaliza el bucle, leer� la siguiente instrucci�n del if, donde comprueba que si la cantidadDivisores es mayor a 2, si lo es, 
		 * entonces
		 * imprimir� el syso de " no lo es". En caso que sea igual o menor, se imprimir� "si lo es" */
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
