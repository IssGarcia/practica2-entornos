package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*El primer error se encuentra en la l�nea 23 cuando queremos acceder a una posici�n -1 
		 * mediante un substring, la soluci�n a este problema ser�a a�adiendo una posici�n m�s a la posici�nCaracter (+1).
		 Aun as� el syso no se llega a imprimir ya que el car�cter guardado en la tabla Ascii en la posici�n 27 no se
		  introduce nunca en la cadena ya que es un s�mbolo que no podemos reproducir.*/

		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter+1);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
