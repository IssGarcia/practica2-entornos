package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*El error lo marca en la l�nea 23 debido a que cuando introduzco el primer int, se me
		 *  almacena en el siguiente string �/n� : el intro introducido. La soluci�n ser�a limpiar el buffer justo debajo del
		 *   n�mero le�do y antes de pasar al siguiente syso.
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		input.nextLine();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
