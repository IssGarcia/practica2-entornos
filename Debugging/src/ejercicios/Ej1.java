package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*El error se encuentra en la l�nea 27, cuando el charAt() intenta acceder a la posici�n 4 dada por la �i� y 
		 * la cual no existe, por lo que sale fuera de rango, ya que solo ir�a de posici�n cero a posici�n 3.
         La soluci�n ser�a eliminando del bucle for el �=� para que no te incluya la �ltima posici�n.*/

		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i < cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
