package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*El error se encuentra cuando la i toma el valor de 0, ya que cuando pasa a la siguiente instrucci�n se realiza la 
divisi�n n/0, por lo que el programa salta ya que no existe ning�n numero divisible para cero.
Una soluci�n ser�a quitar la igualdad a cero en el bucle for.

		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i > 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
