package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import java.awt.Choice;
import java.awt.TextField;
import java.awt.List;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
/**
 * 
 * @author isabel
 * @since 20/12/2017
 */
public class VentanaPrincipal extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaPrincipal frame = new VentanaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaPrincipal() {
		setTitle("ContaSimple");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 721, 496);
		
		JMenuBar menuBar_1 = new JMenuBar();
		setJMenuBar(menuBar_1);
		
		JMenu mnNewMenu = new JMenu("Contabilidad");
		menuBar_1.add(mnNewMenu);
		
		JMenu mnResumen = new JMenu("Resumen");
		mnResumen.setIcon(null);
		mnNewMenu.add(mnResumen);
		
		JMenuItem mntmGraficaDeIngreos = new JMenuItem("Grafica de Ingresos");
		mnResumen.add(mntmGraficaDeIngreos);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Prevision");
		mnResumen.add(mntmNewMenuItem);
		
		JMenu mnFacturasEmitidas = new JMenu("Facturas emitidas");
		mnNewMenu.add(mnFacturasEmitidas);
		
		JMenuItem mntmNuevaFactura = new JMenuItem("Nueva factura");
		mnFacturasEmitidas.add(mntmNuevaFactura);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Gastos");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenuItem mntmBienesDeInversion = new JMenuItem("Bienes de inversion");
		mnNewMenu.add(mntmBienesDeInversion);
		
		JMenuItem mntmAlbaranes = new JMenuItem("Albaranes");
		mnNewMenu.add(mntmAlbaranes);
		
		JMenu mnNewMenu_7 = new JMenu("Presupuestos");
		mnNewMenu.add(mnNewMenu_7);
		
		JMenuItem mntmListar = new JMenuItem("Listar");
		mnNewMenu_7.add(mntmListar);
		
		JMenuItem mntmConfigurar = new JMenuItem("Configurar");
		mnNewMenu_7.add(mntmConfigurar);
		
		JMenu mnNewMenu_1 = new JMenu("Clientes/Prov");
		menuBar_1.add(mnNewMenu_1);
		
		JMenu mnTexto = new JMenu("Clientes");
		mnNewMenu_1.add(mnTexto);
		
		JMenuItem mntmNuevosCliente = new JMenuItem("Nuevo cliente");
		mnTexto.add(mntmNuevosCliente);
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mnTexto.add(mntmBuscar);
		
		JMenu mnProveedores = new JMenu("Proveedores");
		mnNewMenu_1.add(mnProveedores);
		
		JMenuItem mntmNuevo = new JMenuItem("Nuevo proveedor");
		mnProveedores.add(mntmNuevo);
		
		JMenuItem mntmBuscar_1 = new JMenuItem("Buscar");
		mnProveedores.add(mntmBuscar_1);
		
		JMenu mnNewMenu_2 = new JMenu("Catalogo");
		menuBar_1.add(mnNewMenu_2);
		
		JMenuItem mntmNuevoProducto = new JMenuItem("Nuevo producto");
		mnNewMenu_2.add(mntmNuevoProducto);
		
		JMenuItem mntmImportar = new JMenuItem("Importar");
		mnNewMenu_2.add(mntmImportar);
		
		JMenuItem mntmEditar = new JMenuItem("Editar");
		mnNewMenu_2.add(mntmEditar);
		
		JMenuItem mntmImprimir = new JMenuItem("Imprimir");
		mnNewMenu_2.add(mntmImprimir);
		
		JMenu mnNewMenu_3 = new JMenu("Impuestos");
		menuBar_1.add(mnNewMenu_3);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("IVA");
		mnNewMenu_3.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("IRPF");
		mnNewMenu_3.add(mntmNewMenuItem_3);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Calendario fiscal");
		mnNewMenu_3.add(mntmNewMenuItem_4);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Retenciones");
		mnNewMenu_3.add(mntmNewMenuItem_5);
		
		JMenuItem mntmOperacionTerc = new JMenuItem("Operacion Terc.");
		mnNewMenu_3.add(mntmOperacionTerc);
		
		JMenuItem mntmOperacionesIntracomunitarias = new JMenuItem("Operaciones intracomunitarias");
		mnNewMenu_3.add(mntmOperacionesIntracomunitarias);
		
		JMenu mnNewMenu_5 = new JMenu("Disco vitual");
		menuBar_1.add(mnNewMenu_5);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Crear carpeta");
		mnNewMenu_5.add(mntmNewMenuItem_6);
		
		JMenuItem mntmSubirFichero = new JMenuItem("Subir fichero");
		mnNewMenu_5.add(mntmSubirFichero);
		
		JMenuItem mntmAyuda = new JMenuItem("Ayuda");
		mnNewMenu_5.add(mntmAyuda);
		
		JMenu mnNewMenu_6 = new JMenu("Informes");
		menuBar_1.add(mnNewMenu_6);
		
		JMenuItem mntmVentas = new JMenuItem("Ventas");
		mnNewMenu_6.add(mntmVentas);
		
		JMenuItem mntmCompras = new JMenuItem("Compras");
		mnNewMenu_6.add(mntmCompras);
		
		JMenu mnFecha = new JMenu("Fecha");
		mnNewMenu_6.add(mnFecha);
		
		JMenuItem menuItem_2 = new JMenuItem("2015");
		mnFecha.add(menuItem_2);
		
		JMenuItem menuItem_3 = new JMenuItem("2016");
		mnFecha.add(menuItem_3);
		
		JMenuItem menuItem_4 = new JMenuItem("2017");
		mnFecha.add(menuItem_4);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar_1.add(mnAyuda);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setBounds(311, 158, -50, -38);
		contentPane.add(table);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBackground(new Color(176, 224, 230));
		comboBox.setMaximumRowCount(4);
		comboBox.setToolTipText("");
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Venta de mercader\u00EDas", "Prestaci\u00F3n de servicios", "Subvenciones a la explotaci\u00F3n", "Subvenciones al capital", "Ingresos por arrendamiento", "Ingresos financieros"}));
		comboBox.setSelectedIndex(1);
		comboBox.setBounds(136, 179, 329, 20);
		contentPane.add(comboBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1512687600000L), new Date(1512687600000L), new Date(1513292400000L), Calendar.DAY_OF_YEAR));
		spinner.setBounds(155, 117, 62, 20);
		contentPane.add(spinner);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Hombre");
		rdbtnNewRadioButton.setBackground(Color.WHITE);
		rdbtnNewRadioButton.setBounds(100, 227, 69, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JButton btnEnviar = new JButton("Guardar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEnviar.setBounds(606, 384, 89, 23);
		contentPane.add(btnEnviar);
		
		JCheckBox chckbxFondoNegro = new JCheckBox("Sin fecha de vencimiento");
		chckbxFondoNegro.setBackground(Color.WHITE);
		chckbxFondoNegro.setBounds(248, 145, 174, 23);
		contentPane.add(chckbxFondoNegro);
		
		JTextPane txtpnNmero = new JTextPane();
		txtpnNmero.setText("N\u00FAmero:");
		txtpnNmero.setBounds(35, 86, 69, 20);
		contentPane.add(txtpnNmero);
		
		JTextPane txtpnFecha = new JTextPane();
		txtpnFecha.setText("Fecha");
		txtpnFecha.setBounds(35, 117, 62, 20);
		contentPane.add(txtpnFecha);
		
		JTextPane txtpnFechaVencimiento = new JTextPane();
		txtpnFechaVencimiento.setText("Fecha Vencimiento:");
		txtpnFechaVencimiento.setBounds(32, 148, 119, 20);
		contentPane.add(txtpnFechaVencimiento);
		
		JTextPane txtpnTipoDeIngreso = new JTextPane();
		txtpnTipoDeIngreso.setText("Tipo de ingreso:");
		txtpnTipoDeIngreso.setBounds(32, 179, 83, 20);
		contentPane.add(txtpnTipoDeIngreso);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerDateModel(new Date(1512774000000L), new Date(1512774000000L), new Date(1544310000000L), Calendar.DAY_OF_YEAR));
		spinner_1.setBounds(155, 148, 62, 20);
		contentPane.add(spinner_1);
		
		textField = new JTextField();
		textField.setBounds(155, 86, 134, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JTextPane txtpnCliente = new JTextPane();
		txtpnCliente.setText("Cliente:");
		txtpnCliente.setBounds(32, 230, 54, 20);
		contentPane.add(txtpnCliente);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBackground(Color.WHITE);
		rdbtnMujer.setBounds(185, 227, 62, 23);
		contentPane.add(rdbtnMujer);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBackground(new Color(176, 224, 230));
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Buscar clientes"}));
		comboBox_1.setBounds(264, 228, 174, 20);
		contentPane.add(comboBox_1);
		
		JTextPane txtpnAnotaciones = new JTextPane();
		txtpnAnotaciones.setText("Anotaciones:");
		txtpnAnotaciones.setBounds(35, 335, 69, 20);
		contentPane.add(txtpnAnotaciones);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(33, 73, 316, 2);
		contentPane.add(separator);
		
		TextField textField_1 = new TextField();
		textField_1.setBounds(38, 361, 352, 46);
		contentPane.add(textField_1);
		
		JSlider slider = new JSlider();
		slider.setMaximum(1000);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMajorTickSpacing(150);
		slider.setForeground(new Color(0, 0, 255));
		slider.setMinorTickSpacing(100);
		slider.setToolTipText("");
		slider.setValue(550);
		slider.setMinimum(100);
		slider.setBackground(Color.WHITE);
		slider.setBounds(100, 274, 356, 42);
		contentPane.add(slider);
		
		JTextPane txtpnImporte = new JTextPane();
		txtpnImporte.setText("Importe:");
		txtpnImporte.setBounds(32, 274, 48, 20);
		contentPane.add(txtpnImporte);
		
		JButton btnNewButton_1 = new JButton("");
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/impresora.png")));
		btnNewButton_1.setBounds(592, 0, 42, 33);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/rueda-dentada.png")));
		btnNewButton.setBounds(550, 0, 42, 33);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_2 = new JButton("");
		btnNewButton_2.setBackground(Color.WHITE);
		btnNewButton_2.setForeground(Color.WHITE);
		btnNewButton_2.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/pregunta.png")));
		btnNewButton_2.setBounds(513, 0, 42, 33);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setForeground(Color.WHITE);
		btnNewButton_3.setBackground(Color.WHITE);
		btnNewButton_3.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/usuario.png")));
		btnNewButton_3.setBounds(633, 0, 42, 33);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setForeground(Color.WHITE);
		btnNewButton_4.setBackground(Color.WHITE);
		btnNewButton_4.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/correo.png")));
		btnNewButton_4.setBounds(473, 0, 42, 33);
		contentPane.add(btnNewButton_4);
		
		JTextPane txtpnNuevaFactura = new JTextPane();
		txtpnNuevaFactura.setText("Nueva factura");
		txtpnNuevaFactura.setBounds(550, 179, 83, 20);
		contentPane.add(txtpnNuevaFactura);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/anadir.png")));
		btnNewButton_5.setForeground(Color.WHITE);
		btnNewButton_5.setBackground(Color.WHITE);
		btnNewButton_5.setBounds(642, 176, 33, 23);
		contentPane.add(btnNewButton_5);
		
		JTextPane txtpnNuevoCliente = new JTextPane();
		txtpnNuevoCliente.setText("Nuevo cliente");
		txtpnNuevoCliente.setBounds(550, 230, 84, 20);
		contentPane.add(txtpnNuevoCliente);
		
		JButton button = new JButton("");
		button.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/anadir.png")));
		button.setForeground(Color.WHITE);
		button.setBackground(Color.WHITE);
		button.setBounds(642, 227, 33, 23);
		contentPane.add(button);
		
		JTextPane txtpnInformeDeFactura = new JTextPane();
		txtpnInformeDeFactura.setForeground(new Color(25, 25, 112));
		txtpnInformeDeFactura.setFont(new Font("Verdana", Font.BOLD, 15));
		txtpnInformeDeFactura.setText("Informe de factura");
		txtpnInformeDeFactura.setBounds(35, 44, 166, 20);
		contentPane.add(txtpnInformeDeFactura);
		
		JButton btnNewButton_6 = new JButton("");
		btnNewButton_6.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/negrita.png")));
		btnNewButton_6.setBounds(113, 332, 23, 23);
		contentPane.add(btnNewButton_6);
		
		JButton button_1 = new JButton("");
		button_1.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/texto-en-cursiva.png")));
		button_1.setBackground(Color.WHITE);
		button_1.setBounds(136, 332, 23, 23);
		contentPane.add(button_1);
		
		JButton button_2 = new JButton("");
		button_2.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/subrayar-opcion-de-texto.png")));
		button_2.setBackground(Color.WHITE);
		button_2.setBounds(161, 332, 23, 23);
		contentPane.add(button_2);
		
		JButton button_3 = new JButton("");
		button_3.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/alineamiento-a-la-izquierda.png")));
		button_3.setBackground(Color.WHITE);
		button_3.setBounds(185, 332, 23, 23);
		contentPane.add(button_3);
		
		JButton button_4 = new JButton("");
		button_4.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/alineacion-centrada.png")));
		button_4.setBackground(Color.WHITE);
		button_4.setBounds(208, 332, 23, 23);
		contentPane.add(button_4);
		
		JButton button_5 = new JButton("");
		button_5.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/alineacion-a-la-derecha.png")));
		button_5.setBackground(Color.WHITE);
		button_5.setBounds(229, 332, 23, 23);
		contentPane.add(button_5);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 136, 33);
		contentPane.add(toolBar);
		
		JButton btnBzgarb = new JButton("");
		btnBzgarb.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/archivo.png")));
		toolBar.add(btnBzgarb);
		
		JButton btnMjy = new JButton("");
		btnMjy.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/guardar.png")));
		toolBar.add(btnMjy);
		
		JButton btnNewButton_7 = new JButton("");
		btnNewButton_7.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/tijeras.png")));
		toolBar.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		btnNewButton_8.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/copiar-en-el-documento.png")));
		toolBar.add(btnNewButton_8);
		
		JToolBar toolBar_1 = new JToolBar();
		toolBar_1.setBounds(124, 0, 77, 33);
		contentPane.add(toolBar_1);
		
		JButton btnNewButton_9 = new JButton("");
		btnNewButton_9.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha-atras.png")));
		toolBar_1.add(btnNewButton_9);
		
		JButton btnNewButton_10 = new JButton("");
		btnNewButton_10.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/icon.png")));
		toolBar_1.add(btnNewButton_10);
		
		JToolBar toolBar_2 = new JToolBar();
		toolBar_2.setBounds(193, 0, 119, 33);
		contentPane.add(toolBar_2);
		
		JButton btnNewButton_11 = new JButton("");
		btnNewButton_11.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/euro.png")));
		toolBar_2.add(btnNewButton_11);
		
		JButton btnNewButton_12 = new JButton("");
		btnNewButton_12.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/calculador.png")));
		toolBar_2.add(btnNewButton_12);
		
		JButton btnNewButton_13 = new JButton("");
		btnNewButton_13.setIcon(new ImageIcon(VentanaPrincipal.class.getResource("/iconos/icon_calendar.jpg")));
		toolBar_2.add(btnNewButton_13);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
