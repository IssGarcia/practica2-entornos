﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_IsabelGarcia
{
    public class Class2
    {
        public static int[] sumaArrays(int[] array1, int[] array2)
        {
            int[] arraySuma = new int[array1.Length];

            for (int i = 0; i < array2.Length; i++)
            {
                arraySuma[i] = array1[i] + array2[i];
            }

            return arraySuma;
        }



        public static double maximo(double a, double b)
        {
            if (a >= b)
            {
                return a;
            }
            return b;
        }

        public static int aleatorio(int inicio, int fin)
        {
            Random rdn = new Random();

            return (int)(rdn.Next() * (fin - inicio) + inicio);
        }

        public static int redondear(double a)
        {

            if (a - (int)a >= 0.5)
            {
                return (int)a + 1;
            }
            return (int)a;
        }
        public static int potencia(int numBase, int exponente)
        {
            int resultado = 1;

            for (int i = 0; i < exponente; i++)
            {
                resultado = resultado * numBase;
            }
            return resultado;
        }

    }
}

