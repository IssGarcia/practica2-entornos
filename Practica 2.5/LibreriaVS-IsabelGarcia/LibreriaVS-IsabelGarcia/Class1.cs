﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_IsabelGarcia
{
    public class Class1
    {
        public static String invertirCadena(String cadena)
        {

            String cadenaInvertida = "";
            char caracter;
            for (int i = cadena.Length - 1; i >= 0; i--)
            {

                caracter = cadena[i];

                cadenaInvertida = cadenaInvertida + caracter;

            }
            return cadenaInvertida;

        }
        public static bool esEntero(String cadena)
        {


            for (int i = 0; i < cadena.Length; i++)
            {

                if (cadena[0] == '-' && cadena.Length == 1)
                {
                    return false;
                }

                if (i == 0 && cadena[i] != '-' && cadena[i] < '0' && cadena[i] > '9')
                {
                    return false;
                }

                if ((cadena[i] < '0' || cadena[i] > '9') && i != 0)
                {

                }
            }
            return true;
        }




        public static void mitadCadena(String cadena)
        {
            int resultado = cadena.Length/ 2;
             
            String cadenaMitad = cadena.Substring(resultado);
            Console.WriteLine(cadenaMitad);
        }

        public static void contarVocales(String palabra)
        {
            char vocales;
            int contador = 0;
            for (int i = 0; i < palabra.Length; i++)
            {
                vocales = palabra[i];

                if (vocales == 'a' || vocales == 'e' || vocales == 'i' || vocales == 'o' || vocales == 'u')
                {
                    contador++;
                }
            }


            Console.WriteLine("Tiene " + contador + " vocales");



        }



        public static void minusculas(char a, char b)
        {



            bool comprobacion = (a >= 'a' && a <= 'z') || (b >= 'a' && b <= 'z');

            if (comprobacion)
            {
                Console.WriteLine("son minusculas");
            }
            else
            {
                Console.WriteLine("son mayusculas");
            }
        }
    }
}
