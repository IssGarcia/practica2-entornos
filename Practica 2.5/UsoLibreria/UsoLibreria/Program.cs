﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsoLibreria
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array1 = { 3, 4, 6 };
            int[] array2 = { 5, 9, 7 };
            int[] resultado;
            Console.WriteLine(LibreriaVS_IsabelGarcia.Class1.esEntero("32"));
            Console.WriteLine(LibreriaVS_IsabelGarcia.Class1.invertirCadena("ordenador"));
            LibreriaVS_IsabelGarcia.Class1.minusculas('J', 'j');
            LibreriaVS_IsabelGarcia.Class1.contarVocales("manzana");
            LibreriaVS_IsabelGarcia.Class1.mitadCadena("emplazamiento");

            Console.WriteLine(LibreriaVS_IsabelGarcia.Class2.aleatorio(3, 60));
            Console.WriteLine(LibreriaVS_IsabelGarcia.Class2.maximo(27.02, 27.1));
            Console.WriteLine(LibreriaVS_IsabelGarcia.Class2.potencia(3, 3));
            Console.WriteLine(LibreriaVS_IsabelGarcia.Class2.redondear(2.36));
            resultado = LibreriaVS_IsabelGarcia.Class2.sumaArrays(array1, array2);

            for (int i = 0; i < resultado.Length; i++)
            {
                
                    Console.WriteLine(resultado[i]+" ");
                
            }

            Console.ReadKey();
        }
    }
}
