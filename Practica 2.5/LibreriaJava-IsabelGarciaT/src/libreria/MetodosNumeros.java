package libreria;


public class MetodosNumeros {




public static double maximo(double a, double b){
	if( a >= b){
		return a;
	}
	return b;
}

public static int aleatorio(int inicio, int fin){
	
	return (int)(Math.random()* (fin-inicio)) + inicio;
}

public static int redondear(double a){
	
	if(a - (int)a >= 0.5){
		return (int)a + 1;
	}
	return (int)a;
}
public static int potencia(int base, int exponente){
	int resultado = 1;
	
	for(int i = 0; i < exponente; i++){
		resultado = resultado * base;
	}
	return resultado;
}

public static int[] sumaArrays(int[] array1, int[] array2) {
	int[] arraySuma = new int[array1.length];
	
	for (int i = 0; i < array2.length; i++) {
		arraySuma[i] = array1[i] + array2[i];
	}
	
	return arraySuma;
}
	
}
